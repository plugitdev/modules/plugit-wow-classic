const { MessageEmbed } = require("discord.js");
const util = require("../../util");
const request = require("request");
const chalk = require("chalk");
const Database = require("wow-classic-items");
const nexusUrl = "https://api.nexushub.co/wow-classic/v1";
const WarcraftServer = "Faerlina-horde";
var WeightedItems = [];

const lootType = {
  items: new Database.Items(),
  professions: new Database.Professions(),
  zones: new Database.Zones(),
};

function isInteger(value) {
  if (parseInt(value, 10).toString() === value) {
    return true;
  }
  return false;
}

function WeighItem(item, index, arr) {
  let ranNum = Math.floor(Math.random() * 1000 + 1); //generates random number

  item.tempWeight = item.weight * ranNum; //changes the weight of the item in this scope
  WeightedItems.push(item); //pushes item to weighted items array
}

function addPricesReducer(accumulator, a) {
  return accumulator + a;
}

async function getNexusHubMarketPrice(itemID, callback) {
  await request(
    `${nexusUrl}/items/${WarcraftServer}/${itemID}/prices`,
    (error, response, body) => {
      if (error || response.statusCode !== 200) {
        util.logger.error(chalk.red(`Nexushub Got Error: ${body.trim()}`));
      } else {
        data = JSON.parse(body);
        prices = [];
        if (data.data) {
          for (auctionListing of data.data) {
            prices.push(auctionListing.marketValue);
          }

          sum = prices.reduce(addPricesReducer, 0);
          average = Math.round(sum / prices.length);

          callback(average);
        }
      }
    }
  );
}

function isListSmallerThanRequestedItems(obj, num) {
  return;
}

function pickRandomItem(obj, num = 1) {
  if (!obj || isListSmallerThanRequestedItems(obj, num)) {
    return false;
  } else {
    // obj = obj.sort((a, b) => a.itemLevel - b.itemLevel); //Sort the items based on the item level
    return obj[Math.floor(Math.random() * obj.length)]; //Pick a random item from the list
  }
}

function sendInfo(msg, drop) {
  getNexusHubMarketPrice(drop.itemId, (averageMarketPrice) => {
    embed = new MessageEmbed()
      .setColor(drop.colour)
      .setTitle(drop.name)
      .setThumbnail(drop.icon)
      .setTimestamp(); //Create a new embed with the items info and icon

    drop.sellPrice = drop.sellPrice ? `${drop.sellPrice}` : null; //Set the items sell price to null if it doesn't exist

    drop.averageMarketPrice = averageMarketPrice
      ? `${averageMarketPrice} Gold`
      : null; //Add Gold to the end of the Average Market Price

    for (let property of Object.keys(drop)) {
      //For every property in the item
      if (
        !drop[property] ||
        property == "name" ||
        property == "tooltip" ||
        property == "itemLink" ||
        property == "icon" ||
        property == "uniqueName" ||
        property == "source" ||
        property == "contentPhase" ||
        property == "itemId"
      )
        continue; //Skip these properties

      title = property.replace(/([A-Z])/g, " $1");
      title = title.charAt(0).toUpperCase() + title.slice(1); //Generate a pretty title for the embed
      embed.addField(title, drop[property], true); //Add property to the embed
    }

    msg.channel.send(embed);
  });
}

function searchList(list, args) {
  return list.find(
    (o) =>
      o.itemId &&
      (o.uniqueName == args[1] ||
        o.name.includes(args[1]) ||
        o.uniqueName.includes(args[1]))
  );
}

function weighItems(args) {
  lootType[args[0]].forEach(WeighItem);
}

module.exports = {
  commands: {
    wow: {
      name: "wow",
      help: "Drops a random piece of loot from your given loot table.",
      parameters: {
        params: [{ name: "(table)", required: true }, "(quantity)"],
      },
      main: (bot, db, msg) => {
        let args = util.args.parse(msg)["args"];
        if (!args[1]) args[1] = "1"; //If there is no item or quantity specified then set the quantity to 1

        if (lootType[args[0]]) {
          weighItems(args);
        } else {
          // if the table specified doesnt exist
          return msg.reply("Itemlist does not exist");
        }

        if (isInteger(args[1])) {
          //if the second parameter is an interger
          for (let i = 0; i < args[1]; i++) {
            //run this the same number of times as the integer
            let drop = pickRandomItem(WeightedItems, args[1]);
            sendInfo(msg, drop);
          }
        } else if (args[1]) {
          let drop = searchList(WeightedItems, args);
          if (drop && drop.itemId) {
            sendInfo(msg, drop);
          } else {
            msg.channel.send(
              `Could not find any matches for: ${args[1]} in list ${args[0]}`
            );
          }
        }
      },
    },
  },
  events: {},
};
